#ifndef FILE_DATA
#define FILE_DATA
#include <fstream>


struct File
{
	static const int32_t buffer_size_ = 16384; //16 KB
	std::string file_name_;
	std::string folder_path_;
	std::ifstream out_;
	std::ofstream in_;
	char buffer[buffer_size_];
};
#endif // !FILE_DATA

