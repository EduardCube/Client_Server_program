#ifndef FOR_LINUX
#define FOR_LINUX
#ifdef __linux__

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include<errno.h>
#include <unistd.h>
#include<dirent.h>

typedef int SOCKET;
using  SOCKADDR = sockaddr;
#define  SOCKET_ERROR -1
#define INVALID_SOCKET -1

#endif // 
#endif // !FOR_LINUX

