#include "Socket.hpp"


#if defined(__linux__) || defined(_WIN32)
Socket::Socket()
{
}

Socket::Socket(const unsigned port, const string& address)
{
#ifdef _WIN32
	Initialize();
#endif // __MINGW32__

	

	socket_ = Socket::CreateSocket();

	address_.sin_family = AF_INET;
	address_.sin_port = htons(port);

#ifdef _WIN32
	address_.sin_addr.s_addr = inet_addr(address.c_str());
#elif __linux__
	address_.sin_addr.s_addr = htonl(INADDR_ANY);
#endif
}


SOCKET Socket::CreateSocket()
{
	SOCKET new_socket = socket(AF_INET, SOCK_STREAM, 0);  //Addresses - IPv4 TCP-socket
	if (INVALID_SOCKET == new_socket)
	{
#ifdef _WIN32
		cout << "Error with creation socket: " << WSAGetLastError() << endl;
		WSACleanup();
#elif __linux__
		cout << "Error with creation socket: " << errno << endl;
#endif
		exit(Error::SOCKET_);
	}
	return new_socket;
}



#ifdef _WIN32
int Socket::Initialize()
{
	int result = WSAStartup(MAKEWORD(2, 2), &data);
	if (result != 0)
	{
		cout << "Error with initialization Winsock \n Error code" << result << endl;
		exit(Error::INITIALIZE);
	}
	return result;
}
#endif // __MINGW32__


int Socket::Shutdown(SOCKET& socket)
{
#ifdef _WIN32
	int result = shutdown(socket, SD_BOTH);
#elif __linux__
	int result = shutdown(socket, SHUT_RDWR);
#endif
	if (result == SOCKET_ERROR)
	{
#ifdef _WIN32
		cout << "Shutdown failed with error :" << WSAGetLastError() << endl;
		Closesocket(socket);
		WSACleanup();
#elif __linux__
		cout << "Shutdown failed with error :" << errno << endl;
		close(socket);
		
#endif
		
		exit(Error::SHUTDOWN);
	}
	return result;
}



void Socket::Closesocket(SOCKET &socket)
{
#ifdef _WIN32
	closesocket(socket);
#elif __linux__
	close(socket);
#endif
}




Socket::~Socket()
{
	Closesocket(socket_);
}


#endif