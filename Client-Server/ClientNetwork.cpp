#include "ClientNetwork.hpp"

ClientNetwork::ClientNetwork()
{
}

#if defined(__linux__) || defined(_WIN32)
int64_t ClientNetwork::SendFile(SOCKET& socket, const string & file_name)
{
	///////////////////////// open file
#ifdef _WIN32
	string full_path = file_data_.folder_path_ + "\\" + file_name;
#elif __linux__
	string full_path = file_data_.folder_path_ + "/" + file_name;
#endif
	
	file_data_.out_.open(full_path,ios::binary|ios::ate);
	if (!file_data_.out_.is_open())
	{
		cout << "Can't open for reading" << endl;
		return -1;
	}
///////////////////////////////////////////////////////

	int64_t file_size= file_data_.out_.tellg(); // size of file
	int64_t file_offset = 0; 
	int64_t size;
	file_data_.out_.seekg(0); // move cursor to begin of file
	
	SendInt64(socket, file_size); // send file size to server
	int64_t remaining_bytes = 0;
	if (file_size > File::buffer_size_)
	{
		size = File::buffer_size_;
		remaining_bytes = file_size%File::buffer_size_;// file_size % 16384 = remain
	}
	else 
	{
		size = file_size;
	} 
	file_size = file_size - remaining_bytes;

	while (file_offset < file_size) // while we will not receive full file
	{
		file_data_.out_.read(file_data_.buffer, size); // read bytes from file
		SendPacket(socket, file_data_.buffer, size); // receive packet of bytes
		file_offset += size; // increment for condition in order to know when stop
	}
	if (remaining_bytes > 0)
	{
		file_data_.out_.read(file_data_.buffer, remaining_bytes);
		SendPacket(socket, file_data_.buffer, remaining_bytes);
		file_offset += remaining_bytes;
	}
	file_data_.out_.close();
	return file_offset; // return how many bytes we sent
}

bool ClientNetwork::HandlePacketType(SOCKET& socket_,PacketType type)
{
	switch (type)
	{
	case PacketType::MESSAGE:
	{
		string message;
		ReceiveMessage(socket_, message);
		cout << message << endl;
		return true;
	}
	case PacketType::FILE:
	{
		string file_name;
		ReceiveMessage(socket_, file_name);
		Closesocket(socket_);
		SOCKET new_socket;
		new_socket = Socket::CreateSocket(); // reconnecting on new port
		address_.sin_port = htons(7505);
		Connect(new_socket);
		SendPacketType(new_socket, PacketType::FILE);	
		SendFile(new_socket,file_name);
		Closesocket(new_socket);
		return true;
	}
	case PacketType::LIST_OF_FILES:
	{
		SendListOfFiles(socket_, file_data_.folder_path_);
		return true;
	}
	default:
		cout << "Unrecognized type" << endl;
		return false;
	}
	return false;
}

bool ClientNetwork::DirectionExist(const string &direction)
{
#ifdef _WIN32
	DWORD ftyp = GetFileAttributesA(direction.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
#elif __linux__
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(direction.c_str())) != NULL)
	{
		return true;
	}
	return false; // this is not a directory!
#endif
	
}

ClientNetwork::ClientNetwork(const unsigned port, const string &address) : Socket(port, address)
{
	size_ = sizeof(address_);
	Connect(socket_);
}

int ClientNetwork::Connect(SOCKET& socket)
{
	result_ = connect(socket, (SOCKADDR *)&address_, size_);
	if (result_ == -1)
	{
#ifdef _WIN32
		cout << "Unable to connect to server!\n Error code: " << WSAGetLastError() << endl;
		Closesocket(socket);
		WSACleanup();
#elif __linux__
		cout << "Unable to connect to server!\n Error code: " << errno << endl;
		Closesocket(socket);
		
#endif
		
		exit(Error::CONNECT);
	}
	return result_;
}

int ClientNetwork::SendListOfFiles(SOCKET& socket, string& path)
{
	vector<string> files;
	files = ListOfFiles(path); // reveive vector of file-title
	int32_t capasity = files.size();
	SendInt32(socket, capasity); // server needs know how many item he  to receive

	for (auto &it : files)
	{
		SendMEssage(socket, it); // send all file-title list
	}
	return capasity;
}

vector<string> ClientNetwork::ListOfFiles(const string &folder)
{
#ifdef _WIN32
	vector<string> list;
	string search_path = folder + "/*.*";
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(search_path.c_str(), &fd);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				list.push_back(fd.cFileName);
			}
		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}
	return list;
#elif __linux__
	vector<string> list;
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(folder.c_str())) != NULL)
	{
		/* print all the files and directories within directory */
		while ((ent = readdir(dir)) != NULL)
		{
			list.push_back(ent->d_name);
		}
		closedir(dir);
	}
	return list;
#endif
	
}

void ClientNetwork::ClientRun() // main logic of program
{
	ReceivePacketType(socket_); // receive packet type from server
	HandlePacketType(socket_,type_); // handle packet type
	SendPacketType(socket_, PacketType::LIST_OF_FILES); // send packet type to server
	string path;  // for path where contains files
	bool exist = false; // for checking of existing folder
	cout << "Input path to folder in such format: ";
	while (exist == false)  // checking while we don't input correct path
	{
		getline(cin, path);
		exist = DirectionExist(path);
		if(exist==false)
			cout << "Incorrect path try again" << endl;
	}
	file_data_.folder_path_ = path; // save path
	
	SendListOfFiles(socket_,path); // send list of files to server
	ReceivePacketType(socket_);    // receive packet type from server
	HandlePacketType(socket_,type_); // handle that type
}


#endif
ClientNetwork::~ClientNetwork()
{
}
