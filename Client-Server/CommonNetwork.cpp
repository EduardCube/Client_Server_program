#include "CommonNetwork.hpp"



CommonNetwork::CommonNetwork() :packet_size_(sizeof(PacketType))
{

}

#if defined(__linux__) || defined(_WIN32)
int CommonNetwork::SendInt32(SOCKET &socket, int32_t number)
{
	number = htonl(number); // convert from host byte order to network byte order
	SendPacket(socket, (char*)&number, sizeof(int32_t));
	return result_;
}

int CommonNetwork::ReceiveInt32(SOCKET &socket, int32_t& number)
{
	ReceivePacket(socket, (char*)&number, sizeof(int32_t));
	number = ntohl(number);// convert from network byte order to host byte order
	return result_;
}

int CommonNetwork::SendInt64(SOCKET &socket, int64_t number)
{
	number = htonl(number); // convert from host byte order to network byte order
	SendPacket(socket, (char*)&number, sizeof(int64_t));
	return result_;
}

int CommonNetwork::ReceiveInt64(SOCKET &socket, int64_t &number)
{
	ReceivePacket(socket, (char*)&number, sizeof(int64_t));
	number = ntohl(number);// convert from network byte order to host byte order
	return result_;
}

int CommonNetwork::SendPacketType(SOCKET &socket, const PacketType type)
{
	SendInt32(socket, (int32_t)type);
	return result_;
}

int CommonNetwork::ReceivePacketType(SOCKET &socket)
{
	int32_t packetType;
	ReceiveInt32(socket, packetType);
	type_ = (PacketType)packetType;
	return result_;
}

int CommonNetwork::SendPacket(SOCKET& socket, const char* packet, const size_t size)
{
	int iSendBytes = 0;   // total bytes send
	while (iSendBytes < size) // while we still have bytes to send
	{
		result_ = send(socket, packet + iSendBytes, size - iSendBytes, 0);
		if (result_ == -1)   // if there is a socket error
		{
			
#ifdef _WIN32
			cout << "Can't send\n Error code: " << WSAGetLastError() << endl;
			closesocket(socket);
			WSACleanup();
#elif __linux__
			cout << "Can't send\n Error code: " << errno << endl;
			close(socket);
#endif
			exit(Error::SEND);
		}
		iSendBytes += result_;  // add to total bytes send
	}

	return result_;
}


int CommonNetwork::ReceivePacket(SOCKET& socket, char* packet, const size_t size)
{
	int iReceivedBytes = 0;   // total bytes received
	while (iReceivedBytes < size) // while we didn't received all bytes
	{
		result_ = recv(socket, packet + iReceivedBytes, size - iReceivedBytes, 0);
		if (result_ == -1)   // if there is a socket error
		{
			cout << "Error with receiving\n";
#ifdef _WIN32
			cout << "Error code: " << WSAGetLastError() << endl;
			closesocket(socket);
			WSACleanup();
#elif __linux__
			cout << "Error with receiving\n";
			cout << "Error code: " << errno << endl;
			close(socket);
#endif
			exit(Error::RECEIVE);
		}
		iReceivedBytes += result_;  // add to total bytes received
	}

	return result_;
}
int CommonNetwork::SendMEssage(SOCKET& socket, const string& message)
{
	message_length_ = sizeof(message);
	SendInt32(socket, message_length_); // send length of string
	SendPacket(socket, message.c_str(), message_length_);      // send string
	return result_;
}

int CommonNetwork::ReceiveMessage(SOCKET& socket, string& message)
{

	ReceiveInt32(socket, message_length_); // receive size of string
	char* arr = new char[message_length_ + 1];     // create char ptr in order to hold message
	ReceivePacket(socket, arr, message_length_);  // receive message
	arr[message_length_] = '\0';                   //in order to know where stop
	message = arr;
	delete[] arr;
	return result_;
}
#endif
CommonNetwork::~CommonNetwork()
{

}
