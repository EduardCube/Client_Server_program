#include"ClientNetwork.hpp"
using namespace std;


const unsigned port = 7500;
const string address = "127.0.0.1";// broadcast locally
int main()
{
#if defined(__linux__) || defined(_WIN32)
	ClientNetwork client(port,address);
	client.ClientRun();
#endif
	return 0;
}