#ifndef DATA
#define DATA

enum class PacketType
{
	MESSAGE,
	FILE,
	LIST_OF_FILES
};
enum  Error
{
	SOCKET_,
	SHUTDOWN,
	BIND,
	LISTEN,
	ACCEPT,
	SEND,
	RECEIVE,
	INITIALIZE,
	CONNECT
};
#endif // !DATA

