#ifndef CLIENT_NETWORK
#define CLIENT_NETWORK
#include "CommonNetwork.hpp"
#include<vector>


class ClientNetwork : public CommonNetwork, public Socket
{
public:
#if defined(__linux__) || defined(_WIN32)
	ClientNetwork(const unsigned, const string&);
	void ClientRun();
	int64_t SendFile(SOCKET&,const string&);
	bool HandlePacketType(SOCKET&, PacketType) override;
	bool DirectionExist(const string&);
	int SendListOfFiles(SOCKET&,string&);
	vector<string> ListOfFiles(const string&);
	int Connect(SOCKET&);
#endif
	~ClientNetwork();
private:

	/*FUNCTIONS*/
	ClientNetwork();
	

};
#endif // !CLIENT_NETWORK
