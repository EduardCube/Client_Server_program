#ifndef SERVER_NETWORK
#define SERVER_NETWORK
#include "CommonNetwork.hpp"
#include<vector>

class ServerNetwork : public CommonNetwork,public Socket
{
public:
#if defined(__linux__) || defined(_WIN32)
	ServerNetwork(const unsigned, const string&);
	void ServerRun();
	int64_t ReceiveFile(SOCKET&);
	bool DirectionExist(const string&);
	bool RequestFile(SOCKET&, const string&, const string&);
	bool HandlePacketType(SOCKET&, PacketType) override;
	string ChooseFile(const vector<string>&);
	int Bind(SOCKET&);
	int Listen(SOCKET&);
	SOCKET Accept(SOCKET&);
	vector<string> ReceiveListOfFiles(SOCKET&);
#endif
	~ServerNetwork();
private:
	/*FUNCTIONS*/
	ServerNetwork();
	
};
#endif // !SERVER_NETWORK
