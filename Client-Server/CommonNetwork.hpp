#ifndef COMMON_NETWORK
#define COMMON_NETWORK
#include "Socket.hpp"
#include "FileData.hpp"
#include<iostream>
#include<algorithm>
class CommonNetwork
{
public:
	CommonNetwork();

	~CommonNetwork();
protected:

	int size_;
	int64_t result_; // result checking
	PacketType type_;
	int32_t packet_size_;
	int32_t message_length_;
	File file_data_;

	/*FUNCTIONS*/
#if defined(__linux__) || defined(_WIN32)
	virtual bool HandlePacketType(SOCKET&,PacketType)=0;
	int SendInt32(SOCKET&, int32_t);
	int ReceiveInt32(SOCKET&, int32_t&);
	int SendInt64(SOCKET&, int64_t);
	int ReceiveInt64(SOCKET&, int64_t&);
	int SendPacketType(SOCKET&,const PacketType);
	int ReceivePacketType(SOCKET&);
	int SendPacket(SOCKET&, const char*, const size_t);
	int ReceivePacket(SOCKET&, char*, const size_t);
	int SendMEssage(SOCKET&, const string&);
	int ReceiveMessage(SOCKET&, string&);
#endif
};
#endif // !COMMON_NETWORK
