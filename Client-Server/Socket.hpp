#pragma once
#ifndef SOCKET_H
#define SOCKET_H
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#ifdef _WIN32
#include"ForWindows.hpp"
#elif __linux__
#include"ForLinux.hpp"
#endif // 
#include <string>
#include<iostream>
#include "Data.hpp"
using namespace std;


class Socket
{
public:
#if defined(__linux__) || defined(_WIN32)
	Socket();
	Socket(const unsigned,const string&);


	static SOCKET CreateSocket();
	int Shutdown(SOCKET&);
	void Closesocket(SOCKET&);
	~Socket();
#endif

protected:
	
#ifdef __linux__
	SOCKET socket_;
	sockaddr_in address_;
#elif _WIN32
	SOCKET socket_;
	WSADATA data;
	SOCKADDR_IN address_;
	int Initialize();
#endif
private:
	
};
#endif // !Socket
