#include "ServerNetwork.hpp"



ServerNetwork::ServerNetwork()
{
	
}

#if defined(__linux__) || defined(_WIN32)
int64_t ServerNetwork::ReceiveFile(SOCKET& client_socket)
{
	int64_t file_offset = 0;
	int64_t file_size;
	int64_t size;
	ReceiveInt64(client_socket, file_size); // receive file size from client
	int64_t remaining_bytes=0;
	if (file_size > File::buffer_size_) // if file size > our one packet size which is 16 kb
	{
		size = File::buffer_size_;
		remaining_bytes = file_size%File::buffer_size_;
	}
	else {
		size = file_size;
	}
	file_size = file_size - remaining_bytes;
	while (file_offset < file_size) // while we will not receive full file
	{
		ReceivePacket(client_socket, file_data_.buffer, size); // receive packet of bytes
		file_data_.in_.write(file_data_.buffer, size); // write received packet of bytes into file
		file_offset += size; // increment for condition in order to know when stop
	}
	if (remaining_bytes > 0)
	{
		ReceivePacket(client_socket, file_data_.buffer, remaining_bytes);
		file_data_.in_.write(file_data_.buffer, remaining_bytes);
		file_offset += remaining_bytes;
	}
	cout << "File received\n" << endl;
	file_data_.in_.close();
	return file_offset;
}

bool ServerNetwork::DirectionExist(const string & direction)
{
#ifdef _WIN32
	DWORD ftyp = GetFileAttributesA(direction.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
#elif __linux__
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(direction.c_str())) != NULL)
	{
		return true;
	}
	return false; // this is not a directory!
#endif
		
	
}

bool ServerNetwork::RequestFile(SOCKET& client_socket ,const string& folder_path, const string& file)
{
#ifdef _WIN32
	file_data_.in_.open(folder_path + "\\" + file, ios::binary);
#elif __linux__
	file_data_.in_.open(folder_path + "/" + file, ios::binary);
#endif
	if (!file_data_.in_.is_open())
	{
		cout << "Can't open for writing" << endl;
		return false;
	}
	cout << "Requesting file from client " << file << endl;
	SendPacketType(client_socket, PacketType::FILE); // send packet type that we need file
	SendMEssage(client_socket, file); // send title of file to client which we want to receive
	return true;
}

bool  ServerNetwork::HandlePacketType(SOCKET& client_socket,PacketType type)
{
	switch (type)
	{
	case PacketType::MESSAGE:
	{
		string message;
		ReceiveMessage(client_socket, message);
		cout << message << endl;
		return true;
	}
	case PacketType::FILE:
	{
		ReceiveFile(client_socket);
		return true;
	}
	case PacketType::LIST_OF_FILES:
	{
		vector<string> list = ReceiveListOfFiles(client_socket);
		string file = "";
		while (file == "")
		{
			file = ChooseFile(list);
		}
		file_data_.file_name_ = file;
		return true;
	}
	default:
	{
		cout << "Unrecognized type" << endl;
		return false;
	}
	}
	return false;
}



ServerNetwork::ServerNetwork(const unsigned port,const string & address): Socket(port, address)
{
	size_ = (sizeof(address_));
	Bind(socket_);
    Listen(socket_);
}

int ServerNetwork::Bind(SOCKET& socket)
{
	result_ = bind(socket, (SOCKADDR*)&address_, size_);
	if (result_ == -1)
	{
#ifdef _WIN32
		cout << "Binding error :" << WSAGetLastError() << endl;
		Closesocket(socket);
		WSACleanup();
#elif __linux__
		cout << "Binding error :" <<errno << endl;
		Closesocket(socket);
#endif
		
		exit(Error::BIND);
	}
	return result_;
}


int ServerNetwork::Listen(SOCKET& socket)
{
	result_ = listen(socket, SOMAXCONN);
	if (result_ == -1)
	{
#ifdef _WIN32
		cout << "Listening error :" << WSAGetLastError() << endl;
		Closesocket(socket);
		WSACleanup();
#elif __linux__
		cout << "Listening error :" << errno << endl;
		Closesocket(socket);	
#endif
		
		exit(Error::LISTEN);
	}
	return result_;
}

void ServerNetwork::ServerRun() // main logic of program
{
	cout << "Server run\n" << endl;
		SOCKET client_socket = Accept(socket_);
		string folder_path; // path where we will be saving file tha we receive from client
		bool isExist = false; // for checking of existing folder

		SendPacketType(client_socket, PacketType::MESSAGE); // send packet type to client
		SendMEssage(client_socket,"Connected"); // send message for client message that he connected
		ReceivePacketType(client_socket);       // receive packet from client
		HandlePacketType(client_socket,type_); // handle that packet

		cout << "Input folder path where you want to receive file in such format " << endl; // choose folder where we want to save file that we will receive from client
		while (isExist == false)  // checking while we don't input correct path
		{
			getline(cin, folder_path);
			isExist = DirectionExist(folder_path);
			if (isExist == false)
				cout << "Incorrect path try again" << endl;
		}
		RequestFile(client_socket,folder_path, file_data_.file_name_); // open file for receiving packet of bytes from client and send message to client which we want file
		Closesocket(client_socket);  // closing sockets for changing port
		Closesocket(socket_);

		address_.sin_port = htons(7505);  // change port
		SOCKET new_socket;                
		new_socket = Socket::CreateSocket();// creating new socket for server
		Bind(new_socket);
		Listen(new_socket);
		SOCKET new_client; 
		new_client = Accept(new_socket); 
		ReceivePacketType(new_client); // receive packet type from client
		HandlePacketType(new_client,type_); // handle packet
		Closesocket(new_client); 
		Closesocket(new_socket);
}
string ServerNetwork::ChooseFile(const vector<string>& list)
{
	string file;
	if (list.size() == 0)
	{
		cout << "List of files is empty" << endl;
		return "";
	}
	cout << "------------List of files:----------" << endl;
	for (auto& it : list) // show list of files
	{
		cout <<"----  "<< it << endl;
	}
	cout << "Input file which you want to receive: ";
	auto iter = list.end();
	getline(cin,file);
	iter = find(list.begin(), list.end(), file); // check if that file exist
	if (iter == list.end())
	{
		std::cout << "Element not found try again:  \n";
		return "";
	}
	
	return file;
}
SOCKET ServerNetwork::Accept(SOCKET& socket)
{
#ifdef _WIN32
	SOCKET client_socket = accept(socket, (SOCKADDR*)&address_, &size_);
#elif __linux__
	SOCKET client_socket = accept(socket, 0, 0);
#endif
	
	if (client_socket == INVALID_SOCKET)
	{
#ifdef _WIN32
		cout << "Failed to accept client " << WSAGetLastError() << endl;
		Closesocket(socket);
		WSACleanup();
#elif __linux__
		cout << "Failed to accept client " << errno << endl;
		Closesocket(socket);
#endif
		exit(Error::ACCEPT);
	}

	cout << "Client connected \n";
	return client_socket;
}

vector<string> ServerNetwork::ReceiveListOfFiles(SOCKET& client_socket)
{
	int32_t capacity;
	string title;
	ReceiveInt32(client_socket, capacity);

	vector<string> files_list;
	files_list.reserve(capacity);

	for (size_t i = 0; i < capacity; i++)
	{
		ReceiveMessage(client_socket, title);
		files_list.push_back(title);
	}
	return files_list;
}


#endif
ServerNetwork::~ServerNetwork()
{
	
}
